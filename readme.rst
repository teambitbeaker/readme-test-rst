Readme test in reStructuredText
===============================

Subsection with list
--------------------

Type 1:

- A bullet list item

- Second item

  - A sub item

- Third item

Type 2:

1) An enumerated list item

2) Second item

   a) Sub item that goes on at length and thus needs
      to be wrapped. Note the indentation that must
      match the beginning of the text, not the 
      enumerator.

      i) Sub-sub item

3) Third item

Type 3:

#) Another enumerated list item

#) Second item


More stuff
----------

.. image:: https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2012/Nov/04/bitbeaker-logo-1656007062-1_avatar.png

Some text with named link to Bitbeaker_.

.. _Bitbeaker: https://bitbucket.org/bitbeaker-dev-team/bitbeaker/
